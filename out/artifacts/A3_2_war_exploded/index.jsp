<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<h1>Create DVD</h1>

<form method="post" action="dvd/create">
    <div>
        <label>
            Title:
            <input name="title">
        </label>
    </div>

    <div>
        <label>
            Year:
            <input type="number" name="year">
        </label>
    </div>

    <div>
        <label>
            Price:
            <input type="number" name="price">
        </label>
    </div>
    <button type="submit">Create</button>
</form>
</body>
</html>
