package notifier.email;

import com.sun.mail.smtp.SMTPMessage;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import java.util.Properties;

public class EmailNotifier {
    final Properties props;

    public EmailNotifier(){
        props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "805");
    }

    public void send(String body){
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("mihai.muresan13@gmail.com", "ghhaqnotmvqbanhv");
            }
        });

        try {
            SMTPMessage msg = new SMTPMessage(session);
            msg.setFrom(new InternetAddress("mihai.muresan13@gmail.com"));
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("mihai.muresan@agilio.eu"));
            msg.setSubject("New DVD available");
            msg.setText(body);
            msg.setNotifyOptions(SMTPMessage.NOTIFY_SUCCESS);
            int returnOption = msg.getReturnOption();

            System.out.println(returnOption);
            Transport.send(msg);
            System.out.println("sent");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}