package notifier;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import config.QueueConfig;
import notifier.listener.MessageListener;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Start {
    public static void main(String[] argv) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(QueueConfig.HOST);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QueueConfig.NAME, false, false, false, null);
        channel.basicConsume(QueueConfig.NAME, true, new MessageListener(channel));
    }
}
