package notifier.listener;

import admin.entity.DVD;
import com.google.gson.Gson;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import notifier.email.EmailNotifier;

import java.io.IOException;
import java.io.PrintWriter;

public class MessageListener extends DefaultConsumer {
    public MessageListener(Channel channel) {
        super(channel);
    }

    @Override
    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
        String message = new String(body, "UTF-8");
        EmailNotifier emailNotifier = new EmailNotifier();
        emailNotifier.send(message);

        Gson gson = new Gson();
        DVD dvd = gson.fromJson(message, DVD.class);

        PrintWriter printWriter = new PrintWriter("DVD " + dvd.getTitle());
        printWriter.println(message);
        printWriter.close();
    }
}
