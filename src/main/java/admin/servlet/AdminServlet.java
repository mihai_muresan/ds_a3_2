package admin.servlet;

import admin.entity.DVD;
import admin.publisher.MessagePublisher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class AdminServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");
        int year = Integer.parseInt(request.getParameter("year"));
        double price = Double.parseDouble(request.getParameter("price"));

        DVD dvd = new DVD(title, year, price);

        MessagePublisher messagePublisher = new MessagePublisher();
        try {
            messagePublisher.sendNotification(dvd);
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }
}
