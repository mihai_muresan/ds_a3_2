package admin.entity;

import com.google.gson.Gson;

public class DVD {
    private String title;
    private int year;
    private double Price;

    public DVD(String title, int year, double price) {
        this.title = title;
        this.year = year;
        Price = price;
    }

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public double getPrice() {
        return Price;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
