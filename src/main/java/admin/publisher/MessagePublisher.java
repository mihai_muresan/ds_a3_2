package admin.publisher;

import admin.entity.DVD;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import config.QueueConfig;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class MessagePublisher {
    public void sendNotification(DVD dvd) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(QueueConfig.HOST);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QueueConfig.NAME, false, false, false, null);
        String message = dvd.toString();
        channel.basicPublish("", QueueConfig.NAME, null, message.getBytes());
        System.out.println("MessagePublisher: Notification sent");

        channel.close();
        connection.close();
    }
}
